##Dev Status
Video Amigo
##Completed
- VID-164: YouTube Ratings: Condensing the two sections of the new top third (completed making changes in API CRON files).
- VID-199: YT Ratings: URL will load highlighting requested channel in table.
##In Progress
- VID-51: add link to master directory - this task will be completed by 13th May
- VID-175: Channel Directory, Growth Module: Change new channels message - this task will be completed by 13th May
- VID-98: Spider Chart: Final changes to make it operational - this task will be completed by 13th May
- VID-163: YouTube Ratings: Replace confusing Back Button - this task will be completed by 13th May
- VID-148: URGENT Channel Directory, Growth Module: Data Error, March went backward - this task will be completed by 13th May
- VID-160: YouTube Ratings: Add Country Column, Make More Room - this task will be completed by 13th May
##Sandstorm
##Completed
- SAN-220: Unclassified Videos: Allow video to be classified up to 3xs.
- SAN-204: Unclassified Videos: Buttons above table duplicated.
- SAN-206: Unclassified Videos: Alphabetize Country/Language dropdowns.
- SAN-193: Replace Unclassified Videos: Country & Language.
- SAN-195: Video Miner Classification Functionality.
##In Progress
- SAN-201: Unclassified Videos: Add channel status to videos unclassified - this task will be completed by 13th May
List Central
##Completed
- LIS-64: Swap last with second last column.
- LIS-65: TVI category.
- LIS-66: New themes (category).
##In Progress
- LIS-56: fixed position for the authenticate div - this task will be completed by 13th May
##QA Status
##Completed
- Verified VID-164.
- Verified VID-199.
- Verified SAN-220.
- Verified SAN-204.
- Verified SAN-206.
- Verified SAN-193.
- Verified SAN-195.
- Verified LIS-66.
##In Progress
- Verifying VID-163 - this task will be completed by 13th May
- Verifying VID-175 - this task will be completed by 13th May